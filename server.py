from flask import Flask, render_template, flash, redirect, request, make_response, jsonify
from forms import LoginForm
from auth import try_login, identify, verify

app = Flask(__name__)
app.config.from_object('config')

@app.route('/')
@app.route('/index')
def index():
    user = identify(verify())
    if user:
        print(user)
        return render_template('profile.html', profile=user)
    return redirect('auth')

@app.route('/auth', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        return try_login(form)

    return render_template('login.html',
                           title='Sign In',
                           form=form)


if __name__ == "__main__":
    app.run(debug=True)

