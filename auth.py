from flask import Flask, render_template, flash, redirect, request, make_response, jsonify
import jwt
import datetime


app = Flask(__name__)
app.config.from_object('config')

db = {
    'john': {
        'email': 'john.due@mail.com',
        'username': 'john',
        'password': 'qweqwe',
        'full-name': 'John Due',
        'birthday': '01-01-1970'
    }
}

# User class, implement user model and jwt decoder and encoder
class User(object):
    def __init__(self, username, profile):
        self.username = username
        self.profile = profile

    def __str__(self):
        return "User(username='%s')" % self.username

    def encode_auth_token(self, username):

        try:
            payload = {
                'exp': datetime.datetime.utcnow() + datetime.timedelta(days=0, seconds=5),
                'iat': datetime.datetime.utcnow(),
                'sub': username
            }
            return jwt.encode(
                payload,
                app.config.get('SECRET_KEY'),
                algorithm='HS256'
            )
        except Exception as e:
            return e

    @staticmethod
    def decode_auth_token(auth_token):
        """
        Decodes the auth token
        :param auth_token:
        :return: integer|string
        """
        try:
            payload = jwt.decode(auth_token, app.config.get('SECRET_KEY'))
            return payload['sub']
        except jwt.ExpiredSignatureError:
            return 'Signature expired. Please log in again.'
        except jwt.InvalidTokenError:
            return 'Invalid token. Please log in again.'


# function used to verify user exists in DB and password is correct
def getUser(username, password):
    if not (username and password):
        return False

    user = db.get(username, None)
    if not user:
        return False
    if user and user.get('password') == password:
        return User(username, user)
    else:
        return False


# function used to identify logged-in user for private routes, resolved user profile from db
def identify(identity):
    return db.get(identity)

# function that is used to verify user is autenticated based on access_token
# with fallback to token stored as session on Cookie
def verify():

    try:
        token = request.headers.get('access_token', request.headers.get('Cookie', None))
        if token and 'session' in token:
            access_token = token.split('=')[1]
        else:
            access_token = token

        if access_token:
            return User.decode_auth_token(access_token)
        else:
            return None

    except Exception as e:
        print(e)
        responseObject = {
            'status': 'fail',
            'message': 'Try again'
        }
        return make_response(jsonify(responseObject)), 500

# A login handler function, used to get username and password from form data and create access_token
def try_login(form):

    try:
        user = getUser(form.username.data, form.password.data)
        if user:
            auth_token = user.encode_auth_token(user.username)
            if auth_token:
                res = make_response(render_template('profile.html', profile=user.profile))
                res.headers['auth_token'] = auth_token.decode()
                return res, 200
            return redirect('/auth')
        else:
            flash('Incorrect username or password, please try again')
            return redirect('/auth')

    except Exception as e:
        print(e)
        responseObject = {
            'status': 'fail',
            'message': 'Try again'
        }
        return make_response(jsonify(responseObject)), 500

